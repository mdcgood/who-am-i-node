FROM node:12
# 앱 디렉터리 생성

RUN mkdir -p /project/WhoAmIServer
WORKDIR /project/WhoAmIServer

COPY package*.json ./
#COPY config/ ./
#RUN mkdir -p /project/WhoAmIServer/logs

RUN npm install
#RUN npm install -g pm2

# 프로덕션을 위한 코드를 빌드하는 경우
# RUN npm ci --only=production

ENV NODE_ENV production


# 앱 소스 추가
COPY . .

EXPOSE 3000

#log파일 위치
VOLUME /root/.pm2/logs

CMD [ "node", "index.js" ]